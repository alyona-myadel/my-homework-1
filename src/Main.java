public class Main {
    public static void main(String[] args) {
        CoolInterface[] coolBand = new CoolInterface[]{new UserList(),
                new UserListStreamAPI(), new UserMap(), new UserMapStreamAPI()};
        for (CoolInterface cool : coolBand) {
            start(cool);
        }

    }

    private static void start(CoolInterface elements) {
        System.out.println("------------------------------");
        addElements(elements);
        deleteMaxValue(elements);
        deleteMinValue(elements);
        deleteSmallerValues(elements);
        deleteGreaterValues(elements);
        getSumID(elements);
        getThirdElementInOrder(elements);
        getTwoElementsFromSecond(elements);
        getAllItemsWithGreaterId(elements);
        addTextToAllNames(elements);
        addElements(elements);
        sortByTwoFields(elements);
        System.out.println("------------------------------");
    }

    private static void addElements(CoolInterface elements) {
        System.out.println("Add users.");
        elements.addUser(new User("Pavel", 1));
        elements.addUser(new User("Egor", 5));
        elements.addUser(new User("Vlad", 9));
        elements.addUser(new User("Vadim", 6));
        elements.addUser(new User("Alyona", 4));
        elements.addUser(new User("Aliaksei", 3));
        elements.addUser(new User("Aliaksei", 8));
    }

    private static void deleteMaxValue(CoolInterface elements) {
        System.out.println("Delete max value: " + elements.getMaxUserById().toString());
        elements.removeUser(elements.getMaxUserById());
    }

    private static void deleteMinValue(CoolInterface elements) {
        System.out.println("Delete min value: " + elements.getMaxUserById().toString());
        elements.removeUser(elements.getMinUserById());
    }

    private static void deleteSmallerValues(CoolInterface elements) {
        int id = 2;
        System.out.println("Delete users whose ID is less than \"" + id + "\"\nResult:");
        elements.deleteSmallerValues(id);
        elements.print();
    }

    private static void deleteGreaterValues(CoolInterface elements) {
        int id = 7;
        System.out.println("Delete users whose ID is greater than \"" + id + "\"\nResult:");
        elements.deleteGreaterValues(id);
        elements.print();
    }

    private static void getSumID(CoolInterface elements) {
        System.out.println("The sum of all user IDs : " + elements.getSumId());
    }

    private static void getThirdElementInOrder(CoolInterface elements) {
        System.out.println("The third element in order: " + elements.getUser(2).toString());
    }

    private static void getTwoElementsFromSecond(CoolInterface elements) {
        System.out.println("Two elements from the second:");
        for (User user : elements.getSomeUsers(1, 2)) {
            System.out.println(user.toString());
        }
    }

    private static void getAllItemsWithGreaterId(CoolInterface elements) {
        int id = 5;
        System.out.println("All items whose id is greater than \"" + id + "\"");
        for (User user : elements.getUserGreaterIds(5)) {
            System.out.println(user.toString());
        }
    }

    private static void addTextToAllNames(CoolInterface elements) {
        String text = "_1";
        System.out.println("Add text to all names \"" + text + "\"");
        elements.updateAllNameUsers(text);
        elements.print();
    }

    private static void sortByTwoFields(CoolInterface elements) {
        System.out.println("Sort by two fields\nResult:");
        elements.sortUserByName();
        elements.sortUsersById();
        elements.print();
    }

}
