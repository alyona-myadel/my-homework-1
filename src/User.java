import java.util.Comparator;

public class User {
    private String name;
    private int id;

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public static Comparator<User> getIdComparator() {
        return new UserComparator(UserComparator.SortField.ID_FIELD);
    }

    public static Comparator<User> getNameComparator() {
        return new UserComparator(UserComparator.SortField.NAME_FIELD);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "{name: " + name + ", id: " + id + '}';
    }

    private static class UserComparator implements Comparator<User> {
        private SortField sortField;

        UserComparator(SortField sortField) {
            this.sortField = sortField;
        }

        @Override
        public int compare(User firstUser, User secondUser) {
            switch (sortField) {
                case ID_FIELD:
                    return compareByIds(firstUser, secondUser);
                case NAME_FIELD:
                    return compareByNames(firstUser, secondUser);
                default:
                    return 0;
            }
        }

        private int compareByIds(User firstUser, User secondUser) {
            return firstUser.getId() - secondUser.getId();
        }

        private int compareByNames(User firstUser, User secondUser) {
            return firstUser.getName().compareTo(secondUser.getName());
        }

        public enum SortField {
            ID_FIELD,
            NAME_FIELD
        }
    }
}

