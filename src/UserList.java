import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;

public class UserList extends LinkedList<User> implements CoolInterface {

    private List<User> getUsers() {
        return this;
    }

    @Override
    public void addUser(User user) {
        add(user);
    }

    @Override
    public void removeUser(User user) {
        remove(user);
    }

    @Override
    public User getMaxUserById() {
        return Collections.max(this, User.getIdComparator());
    }

    @Override
    public User getMinUserById() {
        return Collections.min(this, User.getIdComparator());
    }

    @Override
    public void deleteSmallerValues(int idUser) {
        for (int i = 0; i < this.size(); ++i) {
            if (getUsers().get(i).getId() < idUser) {
                removeUser(getUsers().get(i));
            }
        }
    }

    @Override
    public void deleteGreaterValues(int idUser) {
        for (int i = 0; i < this.size(); ++i) {
            if (getUsers().get(i).getId() > idUser) {
                removeUser(getUsers().get(i));
            }
        }
    }

    @Override
    public long getSumId() {
        long sumId = 0;
        for (User user : this) {
            sumId += user.getId();
        }
        return sumId;
    }

    @Override
    public User getUser(int index) {
        return this.get(index);
    }

    @Override
    public List<User> getSomeUsers(int index, int numberUsers) {
        LinkedList<User> users = new LinkedList<>();
        for (int i = index; i <= index - 1 + numberUsers; ++i) {
            users.add(this.getUser(i));
        }
        return users;
    }

    @Override
    public void updateAllNameUsers(String text) {
        for (User user : this) {
            user.setName(user.getName().concat(text));
        }
    }

    @Override
    public List<User> sortUsersById() {
        sort(User.getIdComparator());
        return this;
    }

    @Override
    public List<User> sortUserByName() {
        sort(User.getNameComparator());
        return this;
    }

    @Override
    public List<User> searchUsersByName(String characters) {
        LinkedList<User> users = new LinkedList<>();
        for (User user : this) {
            if (checkCharactersInText(user.getName(), characters)) {
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public void print() {
        for (User user : this) {
            System.out.println(user.toString());
        }
    }

    @Override
    public List<User> getUserGreaterIds(int idUser) {
        List<User> users = new LinkedList<>();
        for (int i = 0; i < this.size(); ++i) {
            if (getUsers().get(i).getId() >= idUser) {
                users.add(getUsers().get(i));
            }
        }
        return users;
    }

    private boolean checkCharactersInText(String text, String characters) {
        Pattern p = Pattern.compile(".*" + characters + ".*", CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        return m.matches();
    }
}
